<?php

namespace App\Command;

use App\Constants\AddRecordingConstants;
use App\Entity\Recording;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class AddRecordingCommand extends Command
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName(AddRecordingConstants::NAME)
            ->setDescription(AddRecordingConstants::DESC)
            ->setHelp(AddRecordingConstants::HELP)
            ->addArgument(
                AddRecordingConstants::PATH,
                InputArgument::REQUIRED,
                AddRecordingConstants::ARGUMENT_DESC
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $files = array();

        $recs = glob($input->getArgument(AddRecordingConstants::PATH).AddRecordingConstants::WAV_EXTENSION);

        $io = new SymfonyStyle($input, $output);
        $io->title(AddRecordingConstants::IO_TITLE);
        $io->progressStart(count($recs));

        foreach ($recs as $rec) {
            if ($this->checkIfRecordExists($rec))
                continue;

            $recording = (new Recording())
                ->setFilePath($rec)
                ->setFileSize(filesize($rec))
            ;

            $this->em->persist($recording);
            $io->progressAdvance();

            $files[] = array($rec);
        }

        $this->em->flush();
        $io->progressFinish();

        $table = new Table($output);
        $table
            ->setHeaders(array(AddRecordingConstants::TABLE_HEADER_FILE))
            ->setRows($files)
        ;
        $table->render();

        $io->success(sprintf(AddRecordingConstants::ON_SUCCESS, count($files)));
    }

    private function checkIfRecordExists($path): bool
    {
        return !empty($this->em
            ->getRepository('App:Recording')
            ->findOneBy(array('filePath' => $path))
        );
    }
}
