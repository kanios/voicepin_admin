<?php

namespace App\Controller;

use App\Constants\RecordingControllerConstants;
use Sonata\AdminBundle\Controller\CRUDController;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class RecordingController extends CRUDController
{
    public function playAction($id)
    {
        $object = $this->admin->getSubject();

        if (!$object) {
            throw new NotFoundHttpException(
                sprintf(RecordingControllerConstants::FILE_NOT_FOUND, $id)
            );
        }

        return new BinaryFileResponse($object->getFilePath());
    }
}
