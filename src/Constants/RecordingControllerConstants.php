<?php

namespace App\Constants;

class RecordingControllerConstants
{
    const FILE_NOT_FOUND = 'Unable to find the file with id: %s';
}
