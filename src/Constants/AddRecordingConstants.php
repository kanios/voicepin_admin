<?php

namespace App\Constants;

class AddRecordingConstants
{
    const NAME = 'app:add-recording';
    const DESC = 'Adds new recording';
    const HELP = 'This command allows you to add recording to database';
    const PATH = 'path';
    const ARGUMENT_DESC = 'Directory path';
    const WAV_EXTENSION = '/*.wav';
    const IO_TITLE = 'Importing .wav files';
    const TABLE_HEADER_FILE = 'File';
    const ON_SUCCESS = 'Added %d files to database.';
}