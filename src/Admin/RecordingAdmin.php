<?php

namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class RecordingAdmin extends AbstractAdmin
{
    protected function configureListFields(ListMapper $list)
    {
        $list
            ->add('id')
            ->add('filePath')
            ->add('fileSize')
            ->add('createdAt')
            ->add('_action', null, array(
                'actions' => array(
                    'play' => array(
                        'template' => 'recording_crud/list__action_play.html.twig'
                    )
                )
            ))
        ;
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection
            ->remove('create')
            ->add('play', $this->getRouterIdParameter().'/play')
        ;
    }
}
