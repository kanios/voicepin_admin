# Chiron

The tool made to learn the essentials about developing web applications with Symfony and its Sonata extensions. The software allows you to manage users and upload .wav files to your database through command line.

### Prerequisites

To install and run the application you need to have:

* PHP of version 7.*
* Database

installed on your server.

### Installing

To install application requirements you want to run:


```
composer update
```

To create new super-user just type:


```
php bin/console fos:user:create adminuser --super-admin
```

To build database schema:

```
php bin/console doctrine:schema:update --force
```

### Uploading .wav files

To upload files from your chosen folder you want to use built-in command:

```
php bin/console app:add-recording <path>
```

The route will return with file of id given in your GET.

## Built With

* [Symfony](https://symfony.com/doc/current/index.html) - The web framework used
* [Composer](https://getcomposer.org/doc/) - Dependency Management
* [SonataAdminBundle](https://github.com/sonata-project/SonataAdminBundle) - Used to generate dashboard
* [SonataDoctrineORMAdminBundle](https://github.com/sonata-project/SonataDoctrineORMAdminBundle) - Used to integrate Doctrine ORM with SonataAdminBundle
* [SonataUserBundle](https://github.com/sonata-project/SonataUserBundle) - Used to manage users and their privileges

## Authors

* **Mikołaj Kania** - *Initial work* - [kanios](https://bitbucket.org/kanios)
